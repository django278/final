from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class ToDoItem(models.Model):
    task_name = models.CharField(max_length=50)
    description = models.CharField(max_length=500)
    status = models.CharField(max_length=50, default = "pending")
    date_created = models.DateTimeField("date created", auto_now=False, auto_now_add=False)
    user = models.ForeignKey(User, on_delete=models.RESTRICT, default = "")

class EventItem(models.Model):
    event_name = models.CharField(max_length=50)
    description = models.CharField(max_length=500)
    status = models.CharField(max_length=50, default = "pending")
    date_created = models.DateTimeField("date created", auto_now=False, auto_now_add=False)
    user = models.ForeignKey(User, on_delete=models.RESTRICT, default = "")

