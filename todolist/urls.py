from django.urls import path
from . import views # import views.py. dot is because it's in the same folder

app_name= 'todolist'

urlpatterns = [
    path("index", views.index, name="index"),

#! Tasks
    path("task/<int:todoitem_id>", views.todoitem, name="viewtodoitem"),
    path("add_task", views.add_task, name="add_task"),
    path("task/<int:todoitem_id>/update_task", views.update_task, name="update_task"),
    path('task/<int:todoitem_id>/delete_task', views.delete_task, name="delete_task"),

#! Events
    path("event/<int:event_id>", views.event, name="viewevent"),
    path('add_event', views.add_event, name="add_event"),
    path("event/<int:event_id>/update_event", views.update_event, name="update_event"),
    path('event/<int:event_id>/delete_event', views.delete_event, name="delete_event"),

#! Users
    path("register", views.register, name="register"),
    path("update_user", views.update_user, name="update_user"),
    path("login", views.login_user, name="login"),
    path("logout", views.logout_user, name="logout")
]