from .forms import LoginForm, AddTaskForm, UpdateTaskForm, RegistrationForm, AddEventForm, UpdateEventForm, UpdateUserForm
from django.forms.models import model_to_dict
from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.

from django.http import HttpResponse  

# local import
from .models import ToDoItem, EventItem

# import template
# from django.template import loader

# import built in user model
from django.contrib.auth.models import User

# import
from django.contrib.auth import authenticate, login, logout

from django.utils import timezone


def index(req):
    todoitem_list = ToDoItem.objects.filter(user_id=req.user.id)
    event_list = EventItem.objects.filter(user_id=req.user.id)
    context = {
        "todoitem_list": todoitem_list,
        "event_list": event_list,
        "user": req.user
    }
    return render(req, "index.html", context)


def todoitem(req, todoitem_id):
    # response  = f"You are viewing the details of {todoitem_id}"
    # return HttpResponse(response)

    todoitem = get_object_or_404(ToDoItem, pk=todoitem_id)
    return render(req, "todoitem.html", model_to_dict(todoitem))


def add_task(req):
    context = {}

    if req.method == "POST":
        form = AddTaskForm(req.POST)

        if form.is_valid() == False:
            form = AddTaskForm()
        else:
            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description']

            duplicates = ToDoItem.objects.filter(
                task_name=task_name, user_id=req.user.id)

        if not duplicates:
            ToDoItem.objects.create(
                task_name=task_name, description=description, date_created=timezone.now(), user_id=req.user.id)
            return redirect("todolist:index")
        else:
            context = {
                "error": True
            }

    return render(req, "add_task.html", context)

def event(req, event_id):
    event = get_object_or_404(EventItem, pk=event_id)
    return render(req, "event.html", model_to_dict(event))

def add_event(req):
    context = {}

    if req.method == "POST":
        form = AddEventForm(req.POST)

        if form.is_valid() == False:
            form = AddEventForm()
        else:
            event_name = form.cleaned_data['event_name']
            description = form.cleaned_data['description']

            duplicates = EventItem.objects.filter(
                event_name = event_name, user_id=req.user.id)

        if not duplicates:
            EventItem.objects.create(
                event_name = event_name, description = description, date_created=timezone.now(), user_id=req.user.id)
            return redirect("todolist:index")
        else:
            context = {
                "error": True
            }

    return render(req, "add_event.html", context)

def update_event(req, event_id):
    event = EventItem.objects.filter(pk = event_id)

    context = {
        'user': req.user,
        'event_id': event_id,
        'event_name': event[0].event_name,
        'description': event[0].description,
        'status': event[0].status
    }

    if req.method == "POST":
        form = UpdateEventForm(req.POST)

        if form.is_valid() == False:
            form = UpdateEventForm()
        else: 
            event_name = form.cleaned_data['event_name']
            description = form.cleaned_data['description']
            status = form.cleaned_data['status']
        
        if event: 
            event[0].event_name = event_name
            event[0].description = description
            event[0].status = status

            event[0].save()

            return redirect("todolist:viewevent", event_id=event[0].id)
        else:
            context = {
                "error": True
            }
            
    return render(req, "update_event.html", context)

def update_task(req, todoitem_id):
    todoitem = ToDoItem.objects.filter(pk = todoitem_id)

    context = {
        'user': req.user,
        'todoitem_id': todoitem_id,
        'task_name': todoitem[0].task_name,
        'description': todoitem[0].description,
        'status': todoitem[0].status
    }

    if req.method == "POST":
        form = UpdateTaskForm(req.POST)

        if form.is_valid() == False:
            form = UpdateTaskForm()
        else: 
            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description']
            status = form.cleaned_data['status']
        
        if todoitem: 
            todoitem[0].task_name = task_name
            todoitem[0].description = description
            todoitem[0].status = status

            todoitem[0].save()

            return redirect("todolist:viewtodoitem", todoitem_id=todoitem[0].id)
        else:
            context = {
                "error": True
            }
            
    return render(req, "update_task.html", context)

def delete_event(req, event_id):
    EventItem.objects.filter(pk = event_id).delete()
    return redirect('todolist:index')

def delete_task(req, todoitem_id):
    ToDoItem.objects.filter(pk = todoitem_id).delete()
    return redirect('todolist:index')
        
def register(req):
    context = {}
    users = User.objects.all()
    is_user_registered = False

    user = User()

    if req.method == "POST":
        form = RegistrationForm(req.POST)

        if form.is_valid() == False:
            form = RegistrationForm()
        else:
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            confirm_password = form.cleaned_data['confirm_password']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']

            if password != confirm_password:
                    context = { 
                        'password_match': False
                    }
                    return render(req, 'register.html', context)
            else:
                for registed_user in users:
                    if registed_user.username == username:
                        is_user_registered = True
                        break

                context = {
                    "is_user_registered": is_user_registered
                }

                if is_user_registered == False:
                    user.username = username
                    user.first_name = first_name
                    user.last_name = last_name
                    user.email = email
                    user.set_password(password)
                    user.is_staff = False
                    user.is_active = True

                    user.save()
                    return redirect("todolist:login")

    return render(req, "register.html", context)



def update_user(req):
    user = User.objects.get(pk = req.user.id)
    context = {
        "first_name": user.first_name,
        "last_name": user.last_name
    }
    
    if req.method == "POST":
        form = UpdateUserForm(req.POST)

        if form.is_valid() == False:
            form = UpdateUserForm()
        else: 
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            password = form.cleaned_data['password']
            confirm_password = form.cleaned_data['confirm_password']
        
        if user is not None: 
            if password != confirm_password:
                context = {
                    **context,
                    'password_match': False
                }
                return render(req, "update_user.html", context)
            else:
                user.first_name = first_name
                user.last_name = last_name
                user.set_password(password)
                user.save()
                return redirect("todolist:index")
            
    return render(req, "update_user.html", context)    

def login_user(req):
    context = {

    }

    if req.method == "POST":
        form = LoginForm(req.POST)

        if form.is_valid() == False:
            form = LoginForm()
        else:
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(username=username, password=password)

            if user is not None:
                context = {
                    "username": username,
                    "password": password
                }
                login(req, user)
                return redirect("todolist:index")
            else:
                context = {
                    'error': True
                }

    return render(req, 'login.html', context)


def logout_user(req):
    logout(req)
    return redirect("todolist:index")
